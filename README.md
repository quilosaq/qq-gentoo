Repositorio no oficial de ebuilds

Se pueden usar con Gentoo Linux y otras distribuciones Linux que usen portage.

Sugerencia para usuarios de Gentoo.

1.- Crear el archivo
/etc/portage/repos.conf/qq-gentoo.conf
con el siguiente contenido:
```
[qq-gentoo]
location = /var/db/repos/qq-gentoo
sync-type = git
sync-uri = https://gitlab.com/quilosaq/qq-gentoo.git
auto-sync = yes
priority = 40
```


2.- Obtener el repositorio ejecutando:
emaint sync -r qq-gentoo

