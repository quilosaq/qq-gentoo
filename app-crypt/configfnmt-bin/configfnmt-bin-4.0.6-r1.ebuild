# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg-utils

DESCRIPTION="Utilidad de la FNMT para solicitar claves cripográficas"

HOMEPAGE="https://www.cert.fnmt.es"
SRC_URI="https://descargas.cert.fnmt.es/Linux/configuradorfnmt_4.0.6_amd64.deb"

LICENSE="FNMT-EULA"
SLOT="0"
KEYWORDS="-* ~amd64"

S=${WORKDIR}

src_unpack() {
	einfo "unpack..."

	unpack ${A}

	mkdir ./data
	tar xvf ./data.tar.xz -C ./data
}

src_install() {
	einfo "install..."

	cp -R "${S}/data/usr" "${D}"/

	sed -i s/^Version/#Version./ "${D}"/usr/share/applications/configuradorfnmt.desktop

	sed -i s/\;Application// "${D}"/usr/share/applications/configuradorfnmt.desktop

	mv "${D}"/usr/share/doc/configuradorfnmt "${D}"/usr/share/doc/${PF}
}

pkg_postinst() {
	einfo "postinst..."

	xdg_desktop_database_update
}

pkg_prerm() {
	einfo "prerm..."
}

pkg_postrm() {
	einfo "postrm..."

	xdg_desktop_database_update
}
