# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg-utils desktop

DESCRIPTION="Aplicacion cliente de PC para comunicar con la App Android DNIeSmartConnect"
HOMEPAGE="https://www.dnielectronico.es/portaldnie/PRF1_Cons02.action?pag=REF_1015&id_menu=65"
SRC_URI="https://www.dnielectronico.es/descargas/Apps/DNIeRemote_1.0-5_amd64.zip"

LICENSE="all-rights-reserved"

RESTRICT="bindist mirror"

SLOT="0"
KEYWORDS="~amd64"

IUSE="usb-conexion"

BDEPEND="
app-arch/unzip
dev-util/desktop-file-utils
dev-util/gtk-update-icon-cache"

RDEPEND="
app-accessibility/at-spi2-core:2
app-arch/bzip2:0/1
dev-cpp/atkmm:0
dev-cpp/cairomm:0
dev-cpp/glibmm:2
dev-cpp/gtkmm:3.0
dev-cpp/pangomm:1.4
dev-libs/expat:0
dev-libs/fribidi:0
dev-libs/glib:2
dev-libs/libffi:0/8
dev-libs/libpcre2:0/3
dev-libs/libsigc++:2
dev-libs/openssl:0/1.1
media-gfx/graphite2:0
media-libs/fontconfig:1.0
media-libs/freetype:2
media-libs/harfbuzz:0/6.0.0
media-libs/libepoxy:0
media-libs/libpng:0/16
sys-apps/dbus:0
sys-apps/util-linux:0
sys-devel/gcc:12
sys-libs/glibc:2.2
sys-libs/ncurses:0/6
sys-libs/zlib:0/1
x11-libs/cairo:0
x11-libs/gdk-pixbuf:2
x11-libs/gtk+:3
x11-libs/libX11:0
x11-libs/libXau:0
x11-libs/libXcomposite:0
x11-libs/libXcursor:0
x11-libs/libXdamage:0
x11-libs/libXdmcp:0
x11-libs/libXext:0
x11-libs/libXfixes:0
x11-libs/libXi:0
x11-libs/libXrandr:0
x11-libs/libXrender:0
x11-libs/libxcb:0/1.12
x11-libs/pango:0
x11-libs/pixman:0
usb-conexion? (
dev-util/android-tools[udev] )"

src_unpack() {
einfo "unpack..."

S="${WORKDIR}"

unpack ${A}

ar x DNIeRemoteSetup_1.0-5_amd64.deb data.tar.xz
tar xf data.tar.xz
}

src_install() {
einfo "install..."

#into /usr
dobin usr/bin/dnieremotewizard

dolib.so usr/local/lib/libdnieremotepkcs11.so.0.0.4
dosym -r /usr/lib64/libdnieremotepkcs11.so.0.0.4 /usr/lib64/libdnieremotepkcs11.so.0
dosym -r /usr/lib64/libdnieremotepkcs11.so.0 /usr/lib64/libdnieremotepkcs11.so

domenu usr/share/applications/DNIeRemoteWizard.desktop

insinto /usr/share
doins -r usr/share/icons
}

pkg_postinst() {
einfo "postinst..."

xdg_desktop_database_update
xdg_icon_cache_update

einfo
einfo "Compruebe si necesita activar el soporte usb (USE usb-conexion)"
einfo
einfo "Integración con Firefox: el nombre de la librería es"
einfo "  libdnieremotepkcs11.so ubicada en /usr/lib64/"
einfo
}

pkg_postrm() {
einfo "postrm..."

xdg_desktop_database_update
xdg_icon_cache_update
}
