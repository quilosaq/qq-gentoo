# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Brother DCP-L2530DW printer driver (cups)"
HOMEPAGE="https://www.brother.es/impresoras/impresoras-laser/dcp-l2530dw"
SRC_URI="https://download.brother.com/welcome/dlf103518/dcpl2530dwpdrv-4.0.0-1.i386.deb"

S="${WORKDIR}"

LICENSE="brother-eula GPL-2"

SLOT="0"
KEYWORDS="~amd64"

DEPEND="net-print/cups-filters"

src_unpack() {
	einfo "unpack..."

	ar x "${DISTDIR}"/dcpl2530dwpdrv-4.0.0-1.i386.deb data.tar.gz
	tar xf data.tar.gz
}

src_install() {

	einfo "install..."

	dodir /etc/opt/brother/Printers/DCPL2530DW/inf/

	dodir /opt/brother/Printers/DCPL2530DW/cupswrapper/
	dodir /opt/brother/Printers/DCPL2530DW/inf/
	dodir /opt/brother/Printers/DCPL2530DW/lpd/
	dodir /usr/share/ppd/brother/

	insinto /usr/share/ppd/brother/
	doins opt/brother/Printers/DCPL2530DW/cupswrapper/brother-DCPL2530DW-cups-en.ppd

	exeinto /opt/brother/Printers/DCPL2530DW/cupswrapper/
	doexe opt/brother/Printers/DCPL2530DW/cupswrapper/lpdwrapper
	doexe opt/brother/Printers/DCPL2530DW/cupswrapper/paperconfigml2

	insinto /opt/brother/Printers/DCPL2530DW/inf/
	doins opt/brother/Printers/DCPL2530DW/inf/brDCPL2530DWfunc
	doins opt/brother/Printers/DCPL2530DW/inf/brDCPL2530DWrc

	exeinto /opt/brother/Printers/DCPL2530DW/lpd/
	doexe opt/brother/Printers/DCPL2530DW/lpd/lpdfilter
	doexe opt/brother/Printers/DCPL2530DW/lpd/x86_64/brprintconflsr3

	doexe opt/brother/Printers/DCPL2530DW/lpd/x86_64/rawtobr3

	dosym -r /opt/brother/Printers/DCPL2530DW/inf/brDCPL2530DWrc /etc/opt/brother/Printers/DCPL2530DW/inf/brDCPL2530DWrc

	dosym -r /opt/brother/Printers/DCPL2530DW/cupswrapper/lpdwrapper /usr/libexec/cups/filter/brother_lpdwrapper_DCPL2530DW
}
