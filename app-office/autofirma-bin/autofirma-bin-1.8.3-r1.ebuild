# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

inherit xdg-utils

DESCRIPTION="Aplicacion de firma electronica del Ministerio de Hacienda y AAPP (ES)"
HOMEPAGE="https://administracionelectronica.gob.es/ctt/clienteafirma"

SRC_URI="https://estaticos.redsara.es/comunes/autofirma/1/8/3/AutoFirma_Linux_Debian.zip -> autofirma183.zip"

S="${WORKDIR}"

LICENSE="|| ( GPL-2+ EUPL-1.1 )"
SLOT="0"

KEYWORDS="-* ~amd64"

IUSE="homed-user"

DEPEND="~virtual/jre-11"

BDEPEND="
	~virtual/jre-11
	>=dev-util/desktop-file-utils-0.27
	>=app-misc/ca-certificates-20230311.3.96.1
	>=dev-libs/nss-3.91[utils]
	>=app-arch/unzip-6.0_p27-r1
	homed-user? ( >=sys-apps/systemd-254.8-r1[homed] )
"

pkg_nofetch() {
	einfo
	einfo "Necesaria descarga de forma manual. Descargue desde:"
	einfo "https://estaticos.redsara.es/comunes/autofirma/1/8/3/AutoFirma_Linux_Debian.zip"
	einfo
	einfo "y copielo en su directorio DISTDIR."
	einfo "Renombrelo como autofirma183.zip"
	einfo
}

src_unpack() {
	einfo "unpack..."

	unpack ${A}

	ar x AutoFirma_1_8_3.deb data.tar.gz
	tar xf data.tar.gz
}

src_install() {
	einfo "install..."

	cp -R "${S}"/etc/ "${D}"/
	cp -R "${S}"/usr/ "${D}"/

	rm -R "${D}"/usr/share/doc/AutoFirma
	dodoc "${S}"/usr/share/doc/AutoFirma/*

	gunzip "${D}"/usr/share/doc/${PF}/EUPL-1.1.gz
	gunzip "${D}"/usr/share/doc/${PF}/GPL-2.0.gz

	sed -i '/Version/d' "${D}"/usr/share/applications/afirma.desktop
	sed -i 's/;Signature//' "${D}"/usr/share/applications/afirma.desktop
	sed -i 's/;Utilities//' "${D}"/usr/share/applications/afirma.desktop
}

pkg_preinst() {
	einfo "preinst..."
}

pkg_postinst() {
	einfo "postinst..."

	if use homed-user; then
		USUARIOSHOMED=$(homectl | grep \ active | sed -e 's/\s.*$//')

		cp /etc/passwd passwd.anterior

		for USUARIO in $USUARIOSHOMED; do
			if [[ -z $(grep ^$USUARIO /etc/passwd) ]]; then
				echo $USUARIO:::::/home/$USUARIO: >> /etc/passwd
			fi
		done
	fi

	java -jar /usr/lib/AutoFirma/AutoFirmaConfigurador.jar

	if [[ -f passwd.anterior ]]; then
		cp passwd.anterior /etc/passwd
	fi

	echo "Generacion de certificados"

	if [ -f "/usr/lib/AutoFirma/script.sh" ]; then
		chmod +x /usr/lib/AutoFirma/script.sh

		source /usr/lib/AutoFirma/script.sh
	fi

	echo "Instalacion del certificado CA en el almacenamiento de Firefox y Chrome"

	openssl x509 -inform der -in /usr/lib/AutoFirma/AutoFirma_ROOT.cer -out /usr/lib/AutoFirma/AutoFirma_ROOT.pem

	mv /usr/lib/AutoFirma/AutoFirma_ROOT.pem /usr/lib/AutoFirma/AutoFirma_ROOT.crt

	if [ -d "/usr/share/ca-certificates/AutoFirma" ]; then
		rm -R /usr/share/ca-certificates/AutoFirma/
	fi

	mkdir /usr/share/ca-certificates/AutoFirma/

	if [ ! -d "/usr/local/share/ca-certificates" ] ; then
		mkdir /usr/local/share/ca-certificates/
	fi

	cp /usr/lib/AutoFirma/AutoFirma_ROOT.crt /usr/share/ca-certificates/AutoFirma/AutoFirma_ROOT.crt
	cp /usr/lib/AutoFirma/AutoFirma_ROOT.crt /usr/local/share/ca-certificates/AutoFirma_ROOT.crt
	update-ca-certificates
	echo "Instalacion del certificado CA en el almacenamiento del sistema"

	if [ -d /etc/icecat ] ; then
		if [ ! -d /etc/icecat/pref ] ; then
			mkdir /etc/icecat/pref
		fi
		ln -s /etc/firefox/pref/AutoFirma.js /etc/icecat/pref/AutoFirma.js
	fi
	if [ -d /etc/firefox-esr ] ; then
		if [ ! -d /etc/firefox-esr/pref ] ; then
			mkdir /etc/firefox-esr/pref
		fi
		ln -s /etc/firefox/pref/AutoFirma.js /etc/firefox-esr/pref/AutoFirma.js
	fi

	rm /usr/lib/AutoFirma/script.sh
	rm /usr/lib/AutoFirma/AutoFirma_ROOT.crt

	xdg_desktop_database_update

	einfo
	einfo "NOTA: Si AutoFirma no funciona con usuarios recien creados se debe hacer una"
	einfo "      reinstalación"
	einfo
}

pkg_prerm() {
	einfo "prerm..."

	if [ -f /etc/icecat/pref/AutoFirma.js ] ; then
		rm /etc/icecat/pref/AutoFirma.js
	fi
	if [ -f /etc/firefox-esr/pref/AutoFirma.js ] ; then
		rm /etc/firefox-esr/pref/AutoFirma.js
	fi

	if [ -f "/usr/lib/AutoFirma/uninstall.sh" ]; then
		chmod +x /usr/lib/AutoFirma/uninstall.sh

		source /usr/lib/AutoFirma/uninstall.sh

	echo "Se ha borrado el certificado CA de los almacenes de Firefox"
	fi

	if [ -d "/usr/share/ca-certificates/AutoFirma" ];
	then
		rm -R /usr/share/ca-certificates/AutoFirma
	fi

	if [ -f "/usr/local/share/ca-certificates/AutoFirma_ROOT.crt" ];
	then
		rm /usr/local/share/ca-certificates/AutoFirma_ROOT.crt
	fi

	if [ ! "$(ls /usr/local/share/ca-certificates/)" ]; then
		rmdir /usr/local/share/ca-certificates
	fi

	if [ -f "/usr/lib/AutoFirma/uninstall.sh" ];
	then
		rm /usr/lib/AutoFirma/uninstall.sh
	fi

	if [ -f "/usr/lib/AutoFirma/autofirma.pfx" ];
	then
		rm /usr/lib/AutoFirma/autofirma.pfx
	fi

	if [ -f "/usr/lib/AutoFirma/AutoFirma_ROOT.cer" ];
	then
		rm /usr/lib/AutoFirma/AutoFirma_ROOT.cer
	fi

	update-ca-certificates

	echo "Se ha borrado el certificado CA en el almacenamiento del sistema"
}

pkg_postrm() {
	einfo "postrm..."

	echo "Desinstalación completada con exito"

	xdg_desktop_database_update
}
