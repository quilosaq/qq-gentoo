# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Ebuild para instalar la aplicación AutoFirma a partir de la descarga que
# ofrece el Portal de Administración Electrónica del Gobierno de España

EAPI=7

inherit xdg-utils

DESCRIPTION="Aplicacion de firma electronica del Ministerio de Hacienda y AAPP (ES)"
HOMEPAGE="https://administracionelectronica.gob.es/ctt/clienteafirma"
SRC_URI="https://estaticos.redsara.es/comunes/autofirma/1/7/1/AutoFirma_Linux.zip"

LICENSE="|| ( GPL-2+ EUPL-1.1 )"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
|| (
www-client/firefox:*
www-client/firefox-bin:*
)
virtual/jre:*
dev-util/desktop-file-utils
app-misc/ca-certificates"

DEPEND="
${RDEPEND}
app-arch/unzip
dev-libs/nss[utils]"

src_unpack() {

einfo "unpack..."

if [[ -n ${A} ]]; then
mkdir "${S}"
fi

cd "${S}"
unpack ${A}

mkdir "${S}"/deb/
cd "${S}"/deb/
ar x "${S}"/AutoFirma_1_7_1.deb data.tar.xz

mkdir "${S}"/deb/data/
cd "${S}"/deb/data/
tar xvf "${S}"/deb/data.tar.xz
}

src_install() {
einfo "install..."

dodir /etc/autofirma
cp "${S}"/deb/data/etc/firefox/pref/AutoFirma.js "${D}"/etc/autofirma/

if [[ -d /opt/firefox/defaults/pref ]]; then
dodir /opt/firefox/defaults/pref
dosym ../../../../etc/autofirma/AutoFirma.js /opt/firefox/defaults/pref/AutoFirma.js;
fi

if [[ -d /usr/lib64/firefox/defaults/pref ]]; then
dodir /usr/lib64/firefox/defaults/pref
dosym ../../../../../etc/autofirma/AutoFirma.js /usr/lib64/firefox/defaults/pref/AutoFirma.js;
fi

dodir /usr/share/applications/
cp "${S}"/deb/data/usr/share/applications/afirma.desktop "${D}"/usr/share/applications/

dobin "${S}"/deb/data/usr/bin/AutoFirma

dodir /usr/lib/AutoFirma
cp "${S}"/deb/data/usr/lib/AutoFirma/AutoFirma.jar "${D}"/usr/lib/AutoFirma/

cp "${S}"/deb/data/usr/lib/AutoFirma/AutoFirma.png "${D}"/usr/lib/AutoFirma/

dodir /usr/share/AutoFirma
cp "${S}"/deb/data/usr/share/AutoFirma/AutoFirma.svg "${D}"/usr/share/AutoFirma/

cp "${S}"/deb/data/usr/lib/AutoFirma/AutoFirmaConfigurador.jar "${D}"/usr/lib/AutoFirma/
}

pkg_postinst() {
einfo "postinst..."

xdg_desktop_database_update

java -jar /usr/lib/AutoFirma/AutoFirmaConfigurador.jar

source /usr/lib/AutoFirma/script.sh

openssl x509 -inform der -in /usr/lib/AutoFirma/AutoFirma_ROOT.cer -out /usr/lib/AutoFirma/AutoFirma_ROOT.pem
mv /usr/lib/AutoFirma/AutoFirma_ROOT.pem /usr/lib/AutoFirma/AutoFirma_ROOT.crt
mkdir -p /usr/local/share/ca-certificates/
cp /usr/lib/AutoFirma/AutoFirma_ROOT.crt /usr/local/share/ca-certificates/AutoFirma_ROOT.crt

einfo
einfo "NOTA: Si AutoFirma no funciona con usuarios recien creados se debe hacer una"
einfo "      reinstalación"
einfo
}

pkg_prerm() {
einfo "prerm..."

source /usr/lib/AutoFirma/uninstall.sh

rm /usr/lib/AutoFirma/autofirma.pfx
rm /usr/lib/AutoFirma/AutoFirma_ROOT.cer
rm /usr/lib/AutoFirma/script.sh
rm /usr/lib/AutoFirma/uninstall.sh
rm /usr/lib/AutoFirma/AutoFirma_ROOT.crt
rm /usr/local/share/ca-certificates/AutoFirma_ROOT.crt
}

pkg_postrm() {
einfo "postrm..."

xdg_desktop_database_update
}
